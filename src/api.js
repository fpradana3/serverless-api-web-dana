const express = require("express");
const serverless = require("serverless-http");

const app = express();
const cors = require('cors');
app.use(cors());
const router = express.Router();
var mysql = require('serverless-mysql')({
  library: require('mysql2'),
  config:{
    host: '34.101.177.17',
    user: 'root',
    password: 'passworddbucapan',
    database: 'databaseucapan'
  }
});
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


router.get("/", (req, res) => {
  res.json({
    hello: "hi!"
  });
});

router.get('/ucapan', async function (req, res) {
  await mysql.query('SELECT * FROM ListUcapan', function (error, results, fields) {
  if (error) throw error;
  return res.send({ error: false, data: results, message: 'users list.' });
  });
});

router.post('/addUcapan', async (req, res) => {
  console.log('ini bodynya')
  await console.log(req.body.nama)
  await console.log(req.body.ucapan)
  let nama = req.body.nama;
  if (!nama) {
      return res.status(400).send({ error:true, message: 'Please provide nama' });
  }
  let ucapan = req.body.ucapan;
  if (!ucapan) {
      return res.status(400).send({ error:true, message: 'Please provide ucapan' });
  }
  let query = `INSERT INTO ListUcapan VALUES ('${nama}','${ucapan}')`;
  await mysql.query(query,function (error, results, fields) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: 'New ucapan has been added.' });
  });
});


app.use(`/.netlify/functions/api`, router);

module.exports = app;
module.exports.handler = serverless(app);
